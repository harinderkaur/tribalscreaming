import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestScreaming {

	//Req1- Red Phase: Scream not implemented
	//Green phase: scream is implemented
	  @Test
	  void testOnePersonIsAmazing() {
	  Screaming s = new Screaming();
	  String result = s.scream("peter");
	  assertEquals("Peter is Amazing", result);
	  }
	
	
	  //Req2- green phase implemented but the first test fails now 
        @Test
		void testNobodyIsListening() {
		 Screaming s = new Screaming();
		 String result = s.scream("");
		 assertEquals("You are Amazing", result);
		}
		
      //Req3- GREEN phase implemented 
        @Test
      		void testPeterIsShouting() {
      		 Screaming s = new Screaming();
      		 String result = s.scream("PETER");
      		 assertEquals("PETER IS AMAZING", result);
      		}
      	
        //Req4- Red/Green phase implemented 
        @Test
      		void testTwoPeopleAreShouting() {
      		 Screaming s = new Screaming();
      		 String result = s.scream("Peter , albert");
      		 assertEquals("Peter and albert are amazing", result);
      		}
		
        //Req5- Red phase implemented 
        @Test
      		void testMoreThanTwoPeopleAreShouting() {
      		 Screaming s = new Screaming();
      		 String result = s.scream("Peter , Albert , jk ");
      		 assertEquals("Peter , albert and jk are amazing", result);
      		}
      	
}
